angular.module('app')

  .constant('ADMIN', {
    id: 1
  })

  .constant('AUTH_EVENTS', {
    notAuthenticated: 'auth-not-authenticated'
  })

  .constant('API_PUBLIC', {
    url: 'http://localhost:3002/'
  })

  .constant('API_ENDPOINT', {
    url: 'http://localhost:3002/api' //188.120.241.78
  });
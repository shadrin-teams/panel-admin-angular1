import '../js/jquery-ui-1.12.1.custom/jquery-ui.min.css';
import 'angular-ui-bootstrap/dist/ui-bootstrap-csp.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// Font Awesome
import '../vendors/font-awesome/css/font-awesome.min.css';
// NProgress
import '../vendors/nprogress/nprogress.css';
// iCheck
import '../vendors/iCheck/skins/flat/green.css';
// bootstrap-progressbar
import '../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css';
// JQVMap
import '../vendors/jqvmap/dist/jqvmap.min.css';
// bootstrap-daterangepicker

import '../vendors/bootstrap-daterangepicker/daterangepicker.css';
//  Include Froala Editor styles 

import '../bower_components/froala-wysiwyg-editor/css/froala_editor.min.css';
import '../bower_components/froala-wysiwyg-editor/css/froala_style.min.css';

//  bootstrap-datetimepicker 
import 'bootstrap/dist/css/bootstrap.css';
import '../bower_components/ng-quick-date/dist/ng-quick-date.css';
import '../bower_components/ng-quick-date/dist/ng-quick-date-default-theme.css';

//  Include Froala Editor Plugins styles 
import '../bower_components/froala-wysiwyg-editor/css/plugins/char_counter.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/code_view.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/colors.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/emoticons.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/file.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/fullscreen.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/image_manager.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/image.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/line_breaker.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/table.css';
import '../bower_components/froala-wysiwyg-editor/css/plugins/video.css';

// Custom Theme Style
import '../css/custom.css';
import 'animate.css/animate.css';
import 'angular-toastr/dist/angular-toastr.css';
import '../css/style.css';
import '../css/media_custom.css';

// Overide styles
import '../css/overflow.sass';

// Angular-colorbox
import 'angular-colorbox/themes/dark/colorbox-darktheme.css';
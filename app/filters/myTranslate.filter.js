export default ($translate, $window, errorService) => {

  return key => {
    let translate = $translate.instant(key);
    if( translate === key){
      errorService.sendError('TRANSLATE_ERROR', `Not exists translate for ${key}`, $window.location.hash, {key});
      console.log('отправляю', translate);
    }

    return translate;
  }
};
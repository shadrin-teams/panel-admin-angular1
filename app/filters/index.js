import moduleName from '../module.name';
import myTranslateFilter from './myTranslate.filter';

angular
  .module(moduleName)
  .filter('myTranslate', myTranslateFilter);
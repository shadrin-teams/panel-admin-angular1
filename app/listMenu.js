export default [
  {title: 'MAIN_PAGE', icon: 'fa fa-home', url: 'index'},
  {title: 'MY_FIRMS', icon: 'fa fa-institution', url: 'firms'},
  {title: 'MY_TOURS', icon: 'fa fa-tasks', url: 'tours',
    items: [
    {title: 'TOURS_LIST', icon: 'fa fa-tasks', url: 'tours'}
  ]},

]
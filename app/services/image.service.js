export default class {
  constructor(Upload, $timeout, API_ENDPOINT) {
    this.Upload = Upload;
    this.$timeout = $timeout;
    this.API_ENDPOINT = API_ENDPOINT;
  }

  getImageSizeUrl(image, size) {
    let imageSize = '';
    switch (size) {
      case 2:
        imageSize = 'big';
        break;
      case 3:
        imageSize = 'thumb';
        break;
      default:
        imageSize = '';
    }
    const listParams = image.split('/');
    listParams[listParams.length - 1] = `${imageSize ? imageSize+'_' : ''}${listParams[listParams.length - 1]}`;
    return listParams.join('/');
  }

  //
  saveImage(data, field, imageObj, params) {
    let image = data[field];
    return new Promise((resolve, reject) => {
      if (!imageObj) resolve();

      image.upload = this.Upload.upload({
        url: `${this.API_ENDPOINT.url}/files/${params.section}/${params.itemId}`,
        data: {file: image}
      });

      image.upload
        .then(response => {
          this.$timeout(() => {
            image.result = response.data;
            const imageUrl = `${response.data.data.folder}${response.data.data.file}`;
            resolve(response);
          });
        }, (response) => {
          if (response.status > 0)
            reject(response.status + ': ' + response.data);
        }, (evt) => {
          // Math.min is to fix IE which reports 200% sometimes
          image.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        })
        .catch(error => {
          reject(error)
        })
    });
  }
}
import moduleName from '../module.name';
import userService from './user.service';
import restService from './rest.service';
import authService from './auth.service';
import permitService from './permit.service';
import imageService from './image.service';
import errorService from './error.service';

angular
  .module(moduleName)
  .service('permitService', permitService)
  .service('authService', authService)
  .service('userService', userService)
  .service('errorService', errorService)
  .service('restService', restService)
  .service('imageService', imageService);
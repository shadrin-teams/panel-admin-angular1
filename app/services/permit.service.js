import _ from 'lodash';

export default class {
  constructor(authService) {
    this.user = authService.getUser();
  }

  checkPermit(sectionSlug, method) {
    if (!sectionSlug || !method || !this.user)return false;

    let permitSection = _.find(this.user.rights, {block: sectionSlug}),
      havePermit = false;

    if (permitSection) {
      switch (method) {
        case 'GET' :
          if (permitSection.read) havePermit = true;
          break;
        case 'POST' :
          if (permitSection.create) havePermit = true;
          break;
        case 'PUT' :
          if (permitSection.update) havePermit = true;
          break;
        case 'DELETE' :
          if (permitSection.delete) havePermit = true;
          break;
      }
      return havePermit;
    }

    return false;
  }
}


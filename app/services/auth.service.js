import moment from 'moment';

export default ($q, $http, $timeout, $state, restService) => {
    let LOCAL_TOKEN_KEY = 'token';
    let LOCAL_TOKEN_EXP_DATE_KEY = 'exp_date';
    let LOCAL_USER_KEY = 'user';
    let LOCAL_DOP_KEY = 'WT';
    let authenticated = false;
    let authToken;
    let _this = this;
    let user = {};


    function getUserStorageData() {
      return $q((resolve, reject) => {
        const token = window.localStorage.getItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_KEY}`);
        const tokenDate = window.localStorage.getItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_EXP_DATE_KEY}`);
        const userKey = window.localStorage.getItem(`${LOCAL_DOP_KEY}-${LOCAL_USER_KEY}`);
        const user = userKey && userKey !== 'undefined' ? JSON.parse(userKey) : '';
        resolve({token, tokenDate, user});
      });
    }

    function loadUserCredentials() {
      let token = window.localStorage.getItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_KEY}`);
      let userValue = window.localStorage.getItem(`${LOCAL_DOP_KEY}-${LOCAL_USER_KEY}`);
      user = userValue && userValue !== 'undefined' ? JSON.parse(userValue) : {};
      if (token) {
        useCredentials(token);
      }
    }

    function storeUserCredentials(token, tokenExp, user) {
      console.log('storeUserCredentials', token, tokenExp, user);
      if (token && token !== 'undefined' && user && user !== 'undefined') {
        window.localStorage.setItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_KEY}`, token);
        window.localStorage.setItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_EXP_DATE_KEY}`, tokenExp);
        window.localStorage.setItem(`${LOCAL_DOP_KEY}-${LOCAL_USER_KEY}`, JSON.stringify(user));
        useCredentials(token);

        let timeOut = tokenExp - moment().unix();
        $timeout(() => {
          console.log('Снял авторизацию по времени', 'Прошло - ', timeOut);
          authenticated = false;
          $state.go('login');
        }, timeOut * 1000);
      } else {
        $state.go('login');
      }
    }

    function useCredentials(token) {
      authenticated = true;
      authToken = token;
      console.log('authenticated', authenticated);

      // Set the token as header for your requests!
      $http.defaults.headers.common.Authorization = 'JWT ' + authToken;
    }

    function destroyUserStoreData() {
      authToken = undefined;
      authenticated = false;
      $http.defaults.headers.common.Authorization = undefined;
      window.localStorage.removeItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_KEY}`);
      window.localStorage.removeItem(`${LOCAL_DOP_KEY}-${LOCAL_TOKEN_EXP_DATE_KEY}`);
      window.localStorage.removeItem(`${LOCAL_DOP_KEY}-${LOCAL_USER_KEY}`);
    }

    let register = user => {
      return $q((resolve, reject) => {
        restService.post('signup', user)
          .then(result => {
            if (result.data.success) {
              resolve(result.data.msg);
            } else {
              reject(result.data.msg);
            }
          })
          .catch(error => {
            reject(error);
          })

      });
    };

    let getUser = () => {
      if (!user.id) {
        getUserStorageData()
          .then(data => {
            return data.user;
          })
          .catch(error => {
            throw new Error(error);
          })
      } else {
        return user;
      }
    };

    let refreshToken = () => {
      return new $q((resolve, reject) => {
        restService.get('refreshToken')
          .then(response => {
            storeUserCredentials(response.token, response.token_exp, response.user);

            authenticated = true;
            resolve({status: 200});
          })
          .catch(error => {
            reject({error})
          })
      });

    };

    let login = userData => {
      return $q((resolve, reject) => {
        restService.post('auth', {email: userData.email, password: userData.password})
          .then(result => {
            if (result.data.success) {
              user = result.data.user;
              authenticated = true;
              storeUserCredentials(result.data.token, result.data.token_exp, user);
              resolve({status: 200});
            } else {
              reject(result.data.msg);
            }
          })
          .catch(error => {
            reject(error);
          })
      });
    };

    let logout = () => {
      authenticated = false;
      destroyUserStoreData();
    };

    loadUserCredentials();

    return {
      getUserStorageData: getUserStorageData,
      destroyUserStoreData: destroyUserStoreData,
      refreshToken: refreshToken,
      getUser: getUser,
      login: login,
      register: register,
      logout: logout,
      setAuthenticated: (value) => {
        authenticated = value;
      },
      isAuthenticated: () => {
        return authenticated;
      }
    };
  }
export default class errorService {
  constructor(restService){
    this.restService = restService;
  }

  sendError(errorType, error, url, data) {
    this.restService.post('errors', { errorType, error, url, data})
      .catch(error => {
        throw new Error(error)
      });
  }
}

errorService.$inject = ['restService'];
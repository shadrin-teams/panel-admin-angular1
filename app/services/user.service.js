import avatarDefault from '../../images/avatarDefault.svg';
import avatarMan from '../../images/avatarMan.svg';
import avatarWoman from '../../images/avatarWoman.svg';

export default class {
  constructor(){
  }

  getAvatar(userData) {
    let userAvatar = '';
    if (userData) {
      if (userData.avatar) {
        userAvatar = userData.avatar;
      } else {
        switch (userData.sex) {
          case 1 :
            userAvatar = avatarMan;
            break;
          case 2 :
            userAvatar = avatarWoman;
            break;
          default :
            userAvatar = avatarDefault;
            break;
        }
      }
    }
    return userAvatar;
  }
}


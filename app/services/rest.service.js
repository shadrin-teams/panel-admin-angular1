export default class restService {
  constructor($http, API_ENDPOINT) {
    this.$http = $http;
    this.API_ENDPOINT = API_ENDPOINT;
  }

  send(method, url, data) {//}, headers, transformRequest) {
    console.log('url', url, data);//, headers, transformRequest );
    return this.$http({
      method,
      url: this.API_ENDPOINT.url + '/' + url,
      data
      /*      headers,
       transformRequest*/
    });
  }

  get(url, data) {
    return this.send('GET', url, data);
  }

  put(url, data) {
    return this.send('PUT', url, data);
  }

  post(url, data, headers, transformRequest) {

    return this.send('POST', url, data, headers, transformRequest);
  }

  delete(url, data) {
    return this.send('DELETE', url, data);
  }
}

restService.$inject = ['$http', 'API_ENDPOINT'];
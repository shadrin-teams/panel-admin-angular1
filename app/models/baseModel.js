/*
 @TODO
 Список правил
 Валидация правил
 */

import _ from 'lodash';
import isInit from './rules/init.rule';
import isBoolean from './rules/boolean.rule';
import isString from './rules/string.rule';
import isJson from './rules/json.rule';
import isEmail from './rules/email.rule';

export default class {
  constructor() {
    this.url = '';
    this.errors = {};
    this.translateData = {};
    this.loadedTranslate = false;
    this.rules = {};
    this.placeholders = {};
    this.hints = {};
    this.icons = {};
  }

  getUrl() {
    return this.url;
  }

  getTranslate() {
    return new Promise((resolve, reject) => {
      const translateKey = `PERMIT_ERROR_${this.getUrl()}`.toUpperCase();
      if (!this.loadedTranslate) {
        this.$translate([translateKey])
          .then(data => {
            this.loadedTranslate = true;
            this.translateData = data;
            this.translateData.PERMIT_ERROR = data[translateKey];
            resolve();
          })
      } else {
        resolve();
      }
    });
  }

  findAll() {
    return this.getTranslate()
      .then(() => {
        if (this.permitService.checkPermit(this.url, 'GET')) {
          return this.restService.get(this.getUrl());
        } else {
          return new Promise((resolve, reject) => {
            this.toastr.error(this.translateData.PERMIT_ERROR);
            reject(403);
          })
        }
      });
  }

  find(id, options = {}) {
    return this.getTranslate()
      .then(() => {
        if (this.permitService.checkPermit(this.url, 'GET')) {
          let dopUrl = '';
          if(options.embed){
            dopUrl += `?embed=${options.embed.join(',')}`;
          }
          let url = `${this.getUrl()}/${id}${dopUrl}`;
          return this.restService.get(url);
        } else {
          return new Promise((resolve, reject) => {
            this.toastr.error(this.translateData.PERMIT_ERROR);
            reject(403);
          })
        }
      });
  }

  form(id = null) {
    return this.getTranslate()
      .then(() => {
        if (this.permitService.checkPermit(this.url, 'GET')) {
          return this.restService.get(`${this.getUrl()}/form${id ? '/'+id : ''}`, {id});
        } else {
          return new Promise((resolve, reject) => {
            this.toastr.error(this.translateData.PERMIT_ERROR);
            reject(403);
          })
        }
      });
  }

  delete(id) {
    if(id){
      return this.restService.delete(`${this.getUrl()}/${id}`, {id});
    }
    return false;
  }

  updateSorting(list) {
    const dataFields = this.getOnlyIDAndName(list);
    return this.restService.put(`${this.getUrl()}/sort`, {list: dataFields});
  }

  update(id, data) {
    const dataFields = this.getOnlySaveField(data);
    if (id && !dataFields.id) {
      dataFields.id = id;
    }


    if (this.validate(dataFields)) {
      return this.restService.put(`${this.getUrl()}/${id}`, dataFields);
    } else {
      return new Promise((resolve, reject) => {
        reject(this.errors);
      });
    }
  }

  sendFile(file, data, id) {
    let fd = new FormData();
    // fd.append('file', file);
    const sendData = {
      file: file
    };

    return this.restService.post(`${this.getUrl()}/${data.category}`,
      JSON.stringify(sendData),
      {'Content-Type': 'multipart/form-data'},
      angular.identity
    );
  }

  save(data, id) {
    if (id && !data.id) {
      data.id = id;
    }

    if (this.validate(data)) {
      const sendData = this.getOnlySaveField(data);
      return data.id ? this.restService.put(`${this.getUrl()}/${data.id}`, sendData) : this.restService.post(this.getUrl(), sendData);
    } else {
      return new Promise((resolve, reject) => {
        reject(this.errors);
      });
    }
  }

  getOnlyIDAndName(data = []) {
    let dataOut = [];
    data.forEach((item) => {
      dataOut.push({id: item.id, pos: item.pos});
    });
    return dataOut;
  }

  getOnlySaveField(data) {
    let dataOut = {};
    Object.keys(this.rules).map((field) => {
      if (data[field]) {

        const ruleList = this.rules[field].split('|');
        _.each(ruleList, rule => {
          if (rule === 'save' || field === 'id' ) {
            dataOut[field] = data[field];
          }
        });
      }
    });

    return dataOut;
  }

  validate(data) {
    if (!data)throw new Error('BaseModel.validate: DATA_IS_REQUIRE');

    this.errors = {};
    Object.keys(this.rules).map((field) => {
      if (this.rules[field]) {
        const ruleList = this.rules[field].split('|');
        _.each(ruleList, rule => {
          switch (rule) {

            case 'int' :
              if (data[field] && !isInit(data[field])) {
                if (!this.errors[field]) this.errors[field] = [];
                this.errors[field].push('INCORRECT_TYPE');
              }
              break;

            case 'empty' :
              if (!data[field]) {
                if (!this.errors[field]) this.errors[field] = [];
                this.errors[field].push('FIELD_CAN_NOT_BE_EMPTY');
              }
              break;

            case 'string' :
              if (data[field] && !isString(data[field])) {
                if (!this.errors[field]) this.errors[field] = [];
                this.errors[field].push('INCORRECT_TYPE');
              }
              break;

            case 'boolean' :
              if (data[field] && !isBoolean(data[field])) {
                if (!this.errors[field]) this.errors[field] = [];
                this.errors[field].push('INCORRECT_TYPE');
              }
              break;

            case 'json' :
              if (data[field] && !isJson(data[field])) {
                if (!this.errors[field]) this.errors[field] = [];
                this.errors[field].push('INCORRECT_TYPE');
              }
              break;

            case 'email' :
              if (data[field] && !isEmail(data[field])) {
                if (!this.errors[field]) this.errors[field] = [];
                this.errors[field].push('INCORRECT_TYPE');
              }
              break;

            default :
              if (rule !== 'save')
                throw new Error(`BaseModel: INCORRECT MODEL RULE - ${rule}`);
          }
        });
      }
    });

    return !Object.keys(this.errors).length;
  }
}

// baseModel.$inject = ['$translate', 'permitService', 'toastr'];
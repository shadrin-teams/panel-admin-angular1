import baseModel from './baseModel';

export default class ToursModel extends baseModel {
  constructor(restService) {
    super();
    this.restService = restService;
    this.url = 'tours';
  }
}
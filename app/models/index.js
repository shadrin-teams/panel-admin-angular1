import moduleName from '../module.name';
import toursModel from './tours.model';
import filesModel from './files.model';
import messagesModel from './messages.model';
import './firms/index';

angular
  .module(moduleName)
  .service('messagesModel', messagesModel)
  .service('filesModel', filesModel)
  .service('toursModel', toursModel);
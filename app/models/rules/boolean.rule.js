import _ from 'lodash';

export default value => {
  return _.isBoolean(value);
}
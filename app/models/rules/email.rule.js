export default value => {
  const emailRegular = /^[\w-\.]+@([\w-]+\.)+[a-z]{2,3}$/i;
  return emailRegular.test(value);
}
import _ from 'lodash';

export default value => {
  return _.isObject(value);
}
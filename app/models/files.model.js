import baseModel from './baseModel';

export default class FirmsModel extends baseModel {
  constructor(restService, $translate, permitService, toastr) {
    super();
    this.restService = restService;
    this.$translate = $translate;
    this.permitService = permitService;
    this.toastr = toastr;
    this.url = 'files';

    this.rules = {
      category: 'string|empty|save',
      image: 'string|save',
      folder: 'string|save',
      itemId: 'string|save',
    };
  }
}
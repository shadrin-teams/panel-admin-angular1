import baseModel from './baseModel';

export default class MessagesModel extends baseModel {
  constructor(restService, $translate, permitService, toastr) {
    super();
    this.restService = restService;
    this.$translate = $translate;
    this.permitService = permitService;
    this.toastr = toastr;
    this.url = 'messages';

    this.rules = {
      user_from: 'int|empty|save',
      user_to: 'int|empty|save',
      subject: 'string|empty|save',
      message: 'string|empty|save',
      date: 'int|empty|save',
      data: 'json|save',
      name: 'string|empty',
      phone: 'string|empty',
      email: 'string|email',
      is_new: 'boolean',
      user_from_delete: 'boolean',
      user_to_delete: 'boolean',
      read_date: 'int',
      reply_send: 'boolean',
      reply_send_date: 'int',
    }
  }
}
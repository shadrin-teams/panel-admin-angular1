import baseModel from '../baseModel';

export default class FirmsModel extends baseModel {
  constructor(restService, $translate, permitService, toastr) {
    super();
    this.restService = restService;
    this.$translate = $translate;
    this.permitService = permitService;
    this.toastr = toastr;
    this.url = 'firms';

    this.rules = {
      name: 'string|empty|save',
      user_id: 'int|empty|save',
      country_id: 'string|empty|save',
      city_id: 'string|empty|save',
      description: 'string|save',
      email: 'string|email|empty|save',
      phone: 'string|empty|save',
      image: 'save',
    };

    this.titles = {
      image: 'LOGO',
      name: 'FIRM_NAME',
      country_id: 'COUNTRY',
      city_id: 'CITY',
      description: 'DESCRIPTION',
      email: 'EMAIL',
      phone: 'PHONE',
    };

    this.placeholders = {
      name: 'FIRM_NAME',
      country_id: 'COUNTRY_FIRM',
      city_id: 'CITY_FIRM',
      description: 'DESCRIPTION_ABOUT_FIRM',
      email: 'EMAIL_FOR_NOTIFY',
      phone: 'CONTACT_PHONE',
    };

    this.hints = {
      country_id: 'COUNTRY_FIRM_HINT',
      image: 'COMPANY_LOGO',
      city_id: 'CITY_FIRM_HINT',
      description: 'DESCRIPTION_ABOUT_FIRM_HINT',
      email: 'EMAIL_FOR_NOTIFY_HINT',
      phone: 'CONTACT_PHONE_HINT',
    };
  }
}
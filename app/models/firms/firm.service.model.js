import baseModel from '../baseModel';

export default class FirmServicesModel extends baseModel {
  constructor(restService, $translate, permitService, toastr) {
    super();
    this.restService = restService;
    this.$translate = $translate;
    this.permitService = permitService;
    this.toastr = toastr;
    this.url = 'firm-services';

    this.rules = {
      name: 'string|empty|save',
      firm_id: 'int|empty|save',
      description: 'string|save',
      order: 'int|save',
    };

    this.titles = {
      name: 'SERVICE_NAME',
      description: 'DESCRIPTION',
    };

    this.placeholders = {
      name: 'SERVICE_NAME',
      description: 'DESCRIPTION_ABOUT_SERVICE',
    };

    this.hints = {
      name: 'SERVICE_NAME',
      description: 'DESCRIPTION_ABOUT_SERVICE',
    };
  }
}
import moduleName from '../../module.name';
import firmsModel from './firms.model';
import firmsStep2Model from './firms.step2.model';
import firmUserServiceModel from './firm.user.service.model';
import firmServiceModel from './firm.service.model';

angular
  .module(moduleName)
  .service('firmsModel', firmsModel)
  .service('firmsStep2Model', firmsStep2Model)
  .service('firmServiceModel', firmServiceModel)
  .service('firmUserServiceModel', firmUserServiceModel);
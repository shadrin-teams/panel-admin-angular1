import baseModel from '../baseModel';

export default class FirmUserServicesModel extends baseModel {
  constructor(restService, $translate, permitService, toastr) {
    super();
    this.restService = restService;
    this.$translate = $translate;
    this.permitService = permitService;
    this.toastr = toastr;
    this.url = 'firm-user-services';
    this.firmId = null;
    this.id = null;

    this.rules = {
      name: 'string|empty|save',
      description: 'string|save',
      order: 'save',
      other_name: 'string',
      firm_id: 'int|empty|save',
      user_id: 'int|empty|save',
    };

    this.titles = {
      name: 'SERVICE_NAME',
      other_name: 'OTHER_NAME',
      description: 'DESCRIPTION',
    };

    this.placeholders = {
      name: 'SERVICE_NAME',
      other_name: 'OTHER_SERVICE_NAME',
      description: 'DESCRIPTION_ABOUT_SERVICE',
    };

    this.hints = {
      name: 'SERVICE_NAME',
      description: 'DESCRIPTION_ABOUT_SERVICE',
    };
  }

  getUrl() {
    console.log('this.firmId', this.firmId);
    return `${this.url}/${this.firmId}${this.id ? '/'.this.id : ''}`;
  }
}
import firmsModel from './firms.model';

export default class FirmsStep2Model extends firmsModel {
  constructor(restService, $translate, permitService, toastr) {
    super();
    this.restService = restService;
    this.$translate = $translate;
    this.permitService = permitService;
    this.toastr = toastr;
    this.url = 'firms';

    this.rules = {
      description: 'string|save',
      image: 'save',
    };
  }
}
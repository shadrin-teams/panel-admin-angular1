import Controller from './controller';
import view from './view.pug';

export default function fieldLabelSelectDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      id: '@',
      placeholder: '@',
      name: '@',
      hint: '@',
      required: '@',
      model: '=',
      options: '=',
      object: '=',
      errors: '='
    },
    controller: Controller,
    controllerAs: 'fieldLabelSelectInputCT'
  };
}

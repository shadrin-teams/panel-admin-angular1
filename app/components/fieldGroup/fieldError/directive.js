import view from './view.pug';

export default function errorFieldDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      errors: '='
    },
    controllerAs: 'errorFieldCT',
  };
}
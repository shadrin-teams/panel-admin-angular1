import Controller from './controller';
import view from './view.pug';

export default function fieldLabelImageDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      id: '@',
      placeholder: '@',
      name: '@',
      hint: '@',
      model: '=',
      options: '=',
      object: '=',
      errors: '='
    },
    controller: Controller,
    controllerAs: 'fieldGICT',
  };
}

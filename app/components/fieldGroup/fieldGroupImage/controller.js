export default class FieldGroupImageController {
  constructor($scope, imageService, API_PUBLIC, firmsModel) {
    this.firmsModel = firmsModel;
    $scope.image = '';
    $scope.imageWithUrl = '';
    $scope.delete = () => {
      $scope.image = null;
    };

    $scope.imageUpload = (event, id) => {
      let files = event.target.files;
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        let reader = new FileReader();
        reader.onload = $scope.imageIsLoaded;
        if (file.type.indexOf('image/') !== -1)
          reader.readAsDataURL(file);
      }
    };

    $scope.imageIsLoaded = e => {
      $scope.$apply(() => {
        $scope.image = e.target.result;
        $scope.model[$scope.id] = $scope.image;
      });
    };

    $scope.delete = () => {
      if(!$scope.image){
       //  this.firmsModel.delete()
      }
      $scope.model[$scope.id] = null
    };

    this.$scope = $scope;
    this.imageService = imageService;
    this.API_PUBLIC = API_PUBLIC;
  }

  $onInit() {
    this.$scope.imageWithUrl = this.imageService.getImageSizeUrl(
      `${this.API_PUBLIC.url}${this.$scope.model[this.$scope.id]}`, 3);
    this.$scope.imageBigWithUrl = this.imageService.getImageSizeUrl(
      `${this.API_PUBLIC.url}${this.$scope.model[this.$scope.id]}`, 1);

    const id = this.$scope.id;
    this.$scope.$watch('model[id]', (newUrl)=>{
      if(typeof newUrl === 'string'){
        this.$scope.imageWithUrl = this.imageService.getImageSizeUrl(
          `${this.API_PUBLIC.url}${newUrl}`, 3);
        this.$scope.imageBigWithUrl = this.imageService.getImageSizeUrl(
          `${this.API_PUBLIC.url}${newUrl}`, 1);
      } else {
        this.$scope.imageWithUrl = '';
      }
    });
  }
}

FieldGroupImageController.$inject = ['$scope', 'imageService', 'API_PUBLIC', 'firmsModel'];
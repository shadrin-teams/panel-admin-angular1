import Controller from './controller';
import view from './view.pug';

export default function fieldGroupTextareaDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      required: '@',
      editor: '@',
      placeholder: '@',
      name: '@',
      hint: '@',
      id: '@',
      model: '=',
      object: '=',
      errors: '='
    },
    controller: Controller,
    controllerAs: 'fieldGTCT',
  };
}

export default class FieldGroupTextareaController {
  constructor($scope) {
    $scope.onChange = id => {
      if ($scope.errors && $scope.errors[id]) {
        delete $scope.errors[id];
      }
    };

    $scope.froalaOptions = {
      toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|',
        'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|',
        'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertTable', '|',
        'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|',
        'print', 'spellChecker', 'help', 'html', '|',
        'undo', 'redo']
    }
  }
}

FieldGroupTextareaController.$inject = ['$scope'];

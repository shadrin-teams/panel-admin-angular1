import _ from "lodash";

export default class FieldGroupController {
  constructor($scope) {
    if ($scope.id && $scope.object) {

      if ($scope.object.rules && $scope.object.rules[$scope.id] ) {
        const rules = $scope.object.rules[$scope.id].split('|');
        _.each(rules, item=>{
          if(item === 'empty')$scope.required = true;
        });
      } else {
        $scope.required = false;
      }

      if ($scope.object.placeholders && $scope.object.placeholders[$scope.id] ) {
        $scope.placeholder = $scope.object.placeholders[$scope.id];
      } else {
        $scope.placeholder = '-';
      }

      if ($scope.object.hints && $scope.object.hints[$scope.id] ) {
        $scope.hint = $scope.object.hints[$scope.id];
      } else {
        $scope.hint = '';
      }

      if ($scope.object.titles && $scope.object.titles[$scope.id] ) {
        $scope.name = $scope.object.titles[$scope.id];
      } else {
        $scope.name = '';
      }
    }

    $scope.onChange = id => {
      console.log('delete', id);
      if($scope.errors && $scope.errors[id]){
        delete $scope.errors[id];
      }
    }
  }


}

FieldGroupController.$inject = ['$scope'];

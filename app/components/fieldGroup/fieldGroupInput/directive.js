import Controller from './controller';
import view from './view.pug';

export default function fieldGroupInputDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      id: '@',
      placeholder: '@',
      name: '@',
      hint: '@',
      required: '@',
      model: '=',
      object: '=',
      errors: '='
    },
    controller: Controller,
    controllerAs: 'fieldGroupInputCT'
  };
}

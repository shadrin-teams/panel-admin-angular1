import _ from "lodash";

export default class FieldGroupController {
  constructor($scope) {
     $scope.onChange = id => {
      if($scope.errors && $scope.errors[id]){
        delete $scope.errors[id];
      }
    }
  }
}

FieldGroupController.$inject = ['$scope'];

import view from './view.pug';
import moduleName from '../../module.name';
import Controller from './controller';

import fieldErrorDirective from './fieldError/directive';
import fieldGroupInputDirective from './fieldGroupInput/directive';
import fieldGroupSelectDirective from './fieldGroupSelect/directive';
import fieldGroupTextareaDirective from './fieldGroupTextarea/directive';
import fieldGroupImageDirective from './fieldGroupImage/directive';

angular
  .module(moduleName)
  .directive('fieldError', fieldErrorDirective)
  .directive('fieldGroupInput', fieldGroupInputDirective)
  .directive('fieldGroupSelect', fieldGroupSelectDirective)
  .directive('fieldGroupTextarea', fieldGroupTextareaDirective)
  .directive('fieldGroupImage', fieldGroupImageDirective);

export default function fieldGroupDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      id: '@',
      model: '=',
      type: '@',
      editor: '@',
      options: '=',
      object: '=',
      errors: '='
    },
    controller: Controller,
    controllerAs: 'fieldGroupCT'
  };
}

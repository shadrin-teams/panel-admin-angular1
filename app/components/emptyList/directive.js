import view from './view.pug';

export default function preloaderDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      text: '@emptyList'
    },
    controllerAs: 'emptyListCT',
  };
}

import view from './view.pug';

export default function paginationBlockDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      paginationOptions: '=',
      reloadGrid: '=',
    },
    controllerAs: 'paginationBlockCT',
  };
}

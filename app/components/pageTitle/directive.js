import view from './view.pug';

export default function pageTitleDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      title: '@pageTitle',
      notFind: '@notFind'
    },
    controllerAs: 'pageTitleCT',
  };
}

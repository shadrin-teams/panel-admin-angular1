import _ from 'lodash';
import gridOptions from '../../../config/grid.options';

export default class FirmController {
  constructor(
    $scope, $rootScope, $translate, $state, $stateParams, $q, $timeout, firmsModel, toastr, authService, filesModel,
    Upload
  ) {
    $scope.isLoaded = false;
    $scope.gridActions = {};
    $scope.model = {};
    $scope.files = {};

    this.firmId = null;
    this.firm = null;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$stateParams = $stateParams;
    this.$translate = $translate;
    this.translates = [];
    this.$q = $q;
    this.toastr = toastr;
    this.authService = authService;
    this.firmsModel = firmsModel;
    this.filesModel = filesModel;
    this.Upload = Upload;
    this.gridOptions = gridOptions;
    this.countries = [];
    this.cities = [];
    this.userId = '';
  }

  $onInit() {
    this.firmId = this.$stateParams.id;
    this.userId = this.authService.getUser().id;
    this.$translate(['FIRM_CONTACT_DATA_SUCCEED_SAVED', 'FORM_ARE_FILLED_WITH_ERRORS', 'ERROR_SAVING_FORM_TRY_AGAIN', 'ERROR_SAVING_FORM_TRY_AGAIN'])
      .then((data)=>{
        this.translates = data;
      });
    this.getFirm();
  }

  getFirm() {
    this.firmsModel.form(this.firmId)
      .then((response) => {
        if(response.data.status === 200){
          this.$scope.model = response.data.data.firm;
          this.$scope.model.country_id = String(this.$scope.model.country_id);
          this.$scope.model.city_id = String(this.$scope.model.city_id);
          if (response.data.data && response.data.data.countries && response.data.data.countries.count) {
            this.countries = response.data.data.countries.items;
          }
          if (response.data.data && response.data.data.cities && response.data.data.cities.count) {
            this.cities = response.data.data.cities.items;
          }
        }
        this.$scope.isLoaded = true;
        this.$scope.$apply();
      })
      .catch(error => {
        throw new Error(error);
      });
  }

  save() {
    const promises = [];
    this.$scope.model.user_id = this.userId;
    const cloneData = _.clone(this.$scope.model);

    return this.firmsModel.save(cloneData)
      .then(res => {

        if(res.data.status === 200) {
          this.toastr.success(this.translates.FIRM_CONTACT_DATA_SUCCEED_SAVED);
        } else {
          if (res.data.error) this.firmsModel.errors = res.data.error;
          if (this.firmsModel.errors.length) {
            this.toastr.error(this.translates.FORM_ARE_FILLED_WITH_ERRORS);
          } else {
            this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
          }
        }
      })
      .catch(error => {
        this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
        throw new Error(error);
      });
  }

  next() {
    if (this.$scope.model.id) {
      this.$state.go('firm', {id: this.$scope.model.id, tab: 'about'});
    }
  }
}

FirmController.$inject = ['$scope', '$rootScope', '$translate', '$state', '$stateParams', '$q', '$timeout', 'firmsModel', 'toastr', 'authService', 'filesModel', 'Upload'];
import _ from 'lodash';

export default class step2Controller {
  constructor(
    $scope, $translate, $state, $stateParams, $q, $timeout, firmsStep2Model, toastr, authService, imageService,
    filesModel,
    Upload
  ) {
    $scope.isLoaded = false;
    $scope.gridActions = {};
    $scope.model = {};
    $scope.files = {};

    this.firmId = null;
    this.firm = null;
    this.$scope = $scope;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$stateParams = $stateParams;
    this.$translate = $translate;
    this.translates = [];
    this.$q = $q;
    this.toastr = toastr;
    this.authService = authService;
    this.imageService = imageService;
    this.firmsStep2Model = firmsStep2Model;
    this.filesModel = filesModel;
    this.Upload = Upload;
    this.userId = '';
    this.image = {};
    this.errors = {};
  }

  $onInit() {
    this.firmId = this.$stateParams.id;
    this.userId = this.authService.getUser().id;

    this.$translate(
      ['SUCCEED_SAVE', 'IMAGE_SUCCEED_SAVE', 'IMAGE_FAILED_SAVE', 'THERE_WAS_A_PROBLEM_LOAD_PICTURE', 'ERROR_SAVING_FORM_TRY_AGAIN'])
      .then((data) => {
        this.translates = data;
      });

    const watchModelImage = this.$scope.$watch('this.model.image', (data) => {
      this.image = data;
    });

    this.$scope.$on('$destroy', () => {
      watchModelImage();
    });

    this.getFirm();
  }

  getFirm() {
    this.firmsStep2Model.find(this.firmId, {embed: ['images']})
      .then(response => {
        this.$scope.isLoaded = true;
        this.$scope.model = response.data.data;
        this.$scope.$apply();
      })
      .catch(error => {
        throw new Error(error);
      });
  }


  save() {
    const promises = [];
    this.$scope.model.user_id = this.userId;
    const cloneData = _.clone(this.$scope.model);

    if (this.image) {
      promises.push(
        this.imageService.saveImage(cloneData, 'image', this.image, {section: 'firms', itemId: this.firmId})
          .then(response => {
            if (response && response.data.status === 200) {
              cloneData.image = `${response.data.data.folder}${response.data.data.file}`;
              this.saveInfo(cloneData)
            } else {
              this.toastr.success(this.translates.IMAGE_FAILED_SAVE);
              throw new Error(this.translates.THERE_WAS_A_PROBLEM_LOAD_PICTURE); // there was a problem loading the picture
            }
          })
          .catch(error => {
            console.log('err', error);
          })
      );
    } else {
      promises.push(
        this.saveInfo(cloneData)
      );
    }

    this.$q.all(promises)
      .catch((list) => {
        this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
      })
  }

  saveInfo(cloneData) {
    this.firmsStep2Model.update(this.firmId, cloneData)
      .then((res) => {
        console.log('res', res);
        if (res.data.status === 200) {
          this.$scope.model = res.data.data;
          this.toastr.success(this.translates.SUCCEED_SAVE);
        } else {
          this.errors = res.data.error;
          this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
        }
      })
      .catch((error) => {
        console.log('err', error);
        this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
        throw new Error(error);
      })
  }
}

step2Controller.$inject = ['$scope', '$translate', '$state', '$stateParams', '$q', '$timeout', 'firmsStep2Model', 'toastr', 'authService', 'imageService', 'filesModel', 'Upload'];
import _ from 'lodash';

export default class step4Controller {
  constructor(
    $scope, $translate, $state, $stateParams, firmsModel, authService, API_PUBLIC, imageService
  ) {
    $scope.model = {};
    this.firmId = null;
    this.$scope = $scope;
    this.model = {};
    this.$translate = $translate;
    this.$stateParams = $stateParams;
    this.translates = {};
    this.authService = authService;
    this.firmsModel = firmsModel;
    this.imageService = imageService;
    this.API_PUBLIC = API_PUBLIC;
    this.userId = '';
    this.isLoaded = false;
  }

  $onInit() {
    this.firmId = this.$stateParams.id;
    this.userId = this.authService.getUser().id;
    this.getFirm();
  }

  getFirm() {
    if (this.firmId) {
      this.firmsModel.find(this.firmId, {embed: ['services', 'country', 'city']})
        .then((res) => {
          if (res.data.status === 200) {
            this.model = res.data.data;
            if(this.model.image){
              this.model.image = `${this.API_PUBLIC.url}${this.model.image}`;
            }
            if(this.model.services){
              this.model.services.forEach((item, index)=>{
                this.model.services[index].show = false;
              });
            }
          }
          this.isLoaded = true;
          this.$scope.$apply();
        })
        .catch((error) => {
          this.isLoaded = true;
          throw new Error(error);
        });
    }
  }
}

step4Controller.$inject = ['$scope', '$translate', '$state', '$stateParams', 'firmsModel', 'authService', 'API_PUBLIC', 'imageService'];
import view from './view.pug';
import controller from './controller';

export default function firmAddStep4FormDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      steps: '=',
      active: '=',
    },
    controller,
    controllerAs: 'firmAddStep4Ctrl'
  };
}

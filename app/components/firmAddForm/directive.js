import view from './view.pug';
import controller from './controller';
import './index';

export default function firmAddFormDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      steps: '=',
      active: '=',
    },
    controller,
    controllerAs: 'firmCtrl'
  };
}

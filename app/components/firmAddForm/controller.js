export default class firmAddController {
  constructor($scope, $rootScope, $state, $stateParams, firmsModel) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.firmsModel = firmsModel;
    this.firmId = 0;
    this.tab = 'contact';
    this.isLoaded = true;
  }

  $onInit() {
    this.firmId = this.$stateParams.id || 0;
    this.tab = this.$stateParams.tab || 'contact';

    this.steps = [
      {id: 'contact', name: 'STEP_1', description: 'CONTACT_DATA', baseUrl: {url: 'firm', params: {id: this.firmId}}},
      {id: 'about', name: 'STEP_2', description: 'ABOUT_COMPANY', baseUrl: {url: 'firm', params: {id: this.firmId}}},
      {
        id: 'services',
        name: 'STEP_3',
        description: 'COMPANY_SERVICES',
        baseUrl: {url: 'firm', params: {id: this.firmId}}
      },
      {id: 'confirm', name: 'STEP_4', description: 'CONFIRMATION', baseUrl: {url: 'firm', params: {id: this.firmId}}}
    ];

    if (!this.firmId) this.tab = 'contact';

    const eventContactSaved = this.$rootScope.$on('CONTACT_DATA_SAVED', (event, data) => {
      console.log('CONTACT_DATA_SAVED', data);
      if (data.id) {
        this.$state.go('firm', {id: data.id, tab: 'about'});
      }
    });

    this.$scope.$on('$destroy', () => {
      eventContactSaved();
    });
  }
}

firmAddController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', 'firmsModel'];
import _ from 'lodash';
import deleteModal from '../../modals/deleteModal.pug';
import addFormController from './addForm/controller';
import addFormView from './addForm/view.pug';

export default class step3Controller {
  constructor(
    $scope, $rootScope, $translate, $state, $stateParams, $uibModal, $timeout, firmUserServiceModel, firmServiceModel,
    toastr,
    authService
  ) {
    $scope.isLoaded = false;
    $scope.gridActions = {};
    $scope.model = {};
    $scope.files = {};

    this.firmId = null;
    this.firm = null;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.model = {};
    this.$state = $state;
    this.$timeout = $timeout;
    this.$uibModal = $uibModal;
    this.$stateParams = $stateParams;
    this.$translate = $translate;
    this.translates = {};
    this.toastr = toastr;
    this.authService = authService;
    this.firmUserServiceModel = firmUserServiceModel;
    this.firmServiceModel = firmServiceModel;
    this.userId = '';
    this.firmServicesList = [];
    this.firmUserServicesList = [];
  }

  $onInit() {
    this.firmId = this.$stateParams.id;
    this.userId = this.authService.getUser().id;
    this.firmUserServiceModel.firmId = this.$stateParams.id;
    this.getFirmUserServices();
    this.$translate([
      'OTHER_SERVICE',
      'YOU_REALLY_WONT_DELETE_SERVICE',
      'SERVICE_DELETED',
      'SERVICE_DELETE_ERROR',
      'ORDER_SAVE_SUCCESSFUL'
    ])
      .then((data) => {
        this.translates = data;
      });

    this.sortableOptions = {
      update: (e, ui) => {
        this.$timeout(() => {
          this.saveOrder();
        });
      },
      placeholder: 'ui-sortable-line-placeholder',
      axis: 'y'
    };

    this.$rootScope.$on("refreshServiceList", ::this.getFirmUserServices)
  }

  saveOrder() {
    if (this.firmUserServicesList.length) {
      const listItems = [];
      const test = _.clone(this.firmUserServicesList);

      this.firmUserServicesList.forEach((item, index) => {
        listItems.push({id: item.id, pos: index+1});
      });

      this.firmUserServiceModel.updateSorting(listItems)
        .then((data) => {
          this.toastr.success(this.translates.ORDER_SAVE_SUCCESSFUL);
        })
        .catch((error) => {
          throw new Error(error)
        });
    }
  }

  add() {
    this.editForm();
  }

  onDrop(target, source) {
    alert('dropped ' + source + ' on ' + target);
  };

  getFirmUserServices() {
    this.isLoaded = false;
    console.log('1', this.firmUserServiceModel);
    this.firmUserServiceModel.findAll()
      .then(response => {
        this.firmUserServicesList = [];
        response.data.items.forEach((item, index) => {
          this.firmUserServicesList.push({...item, article_order: (index + 1), show: false});
        });
        this.isLoaded = true;
        this.$scope.$apply();
      })
      .catch(error => {
        this.isLoaded = true;
        throw new Error(error);
      });
  }

  toggleShow(item) {
    item.show = !item.show;
  }

  editForm(id) {
    const modal = this.$uibModal.open({
      animation: true,
      template: addFormView,
      controller: addFormController,
      controllerAs: 'addFormCtrl',
      windowTopClass: 'in-modal',
      size: 'lg',
      resolve: {
        data: () => ({
          id: (id ? id : null),
          firmId: this.firmId,
          userId: this.userId,
        })
      }
    });
    modal.result.catch((res) => {
      if (!(res === 'cancel' || res === 'escape key press' || res === 'backdrop click')) throw res;
    });
  }

  deleteService(id, name) {
    if (id) {
      const modal = this.$uibModal.open({
        animation: true,
        template: deleteModal,
        controller: () => {
        },
        windowTopClass: 'in-modal',
        size: 'sm',
        resolve: {
          item: () => ({
            id,
            title: `${this.translates.YOU_REALLY_WONT_DELETE_SERVICE} "${name}"?`,
            cancelCB: () => {
              modal.close();
            },
            deleteCB: () => {
              this.firmServiceDelete(modal, id);
            }
          })
        }
      });
      modal.result.catch((res) => {
        if (!(res === 'cancel' || res === 'escape key press' || res === 'backdrop click')) throw res;
      });
    }
  }

  firmServiceDelete(modal, id) {
    if (id) {
      modal.close();
      this.isLoaded = false;
      console.log('this.firmUserServiceModel', this.firmUserServiceModel);
      this.firmUserServiceModel.delete(id)
        .then((res) => {
          if (res.data.status === 200) {
            this.toastr.success(this.translates.SERVICE_DELETED);
            this.getFirmUserServices();
          } else {
            this.toastr.error(this.translates.SERVICE_DELETE_ERROR);
            this.isLoaded = true;
          }
        })
        .catch((error) => {
          console.log('data', error);
          this.isLoaded = true;
          throw new Error(error);
        })
    }
  }

  next() {
    if (this.firmUserServicesList.length) {
      this.$state.go('firm', {id: this.firmId, tab: 'confirm'});
    } else {
      this.toastr.error('ATTENTION', 'YOU_MUST_ADD_AT_LEAST_ONE_SERVICE');
    }
  }
}

step3Controller
  .$inject = ['$scope', '$rootScope', '$translate', '$state', '$stateParams', '$uibModal', '$timeout', 'firmUserServiceModel', 'firmServiceModel', 'toastr', 'authService'];
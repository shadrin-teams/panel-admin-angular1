import _ from 'lodash';

export default class addFormController {
  constructor(
    $scope, $rootScope, $translate, $uibModalInstance, $q, firmUserServiceModel, firmServiceModel, toastr
  ) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$translate = $translate;
    this.$uibModalInstance = $uibModalInstance;
    this.$q = $q;
    this.firmUserServiceModel = firmUserServiceModel;
    this.firmUserServiceModel.firmId = $scope.$resolve.data.firmId;
    this.firmServiceModel = firmServiceModel;
    this.isLoaded = false;
    this.toastr = toastr;
    this.firmServicesList = [];
    this.model = {};
    this.errors = {};
    this.id = $scope.$resolve.data.id;
    this.firmId = $scope.$resolve.data.firmId;
    this.userId = $scope.$resolve.data.userId;
  }

  $onInit() {
    const inquiry = [];
    inquiry.push(this.getTranslate());
    inquiry.push(this.getFirmServices());
    inquiry.push(this.getFirmUserServices());

    this.$q.race(inquiry)
      .then((resItems) => {
        this.isLoaded = true;
      })
      .catch((error) => {
        this.isLoaded = true;
        throw new Error(error);
      });

  }

  getTranslate() {
    this.$translate([
      'OTHER_SERVICE',
      'ERROR_SAVING_FORM_TRY_AGAIN',
      'SUCCESSED_SAVE'
    ])
      .then((data) => {
        this.translates = data;
      });
  }

  getFirmServices() {
    console.log('getFirmServices');
    this.firmServiceModel.findAll()
      .then((res) => {
        console.log('res', res);
        if (res.data.status === 200) {
          this.firmServicesList = [];
          res.data.items.forEach((item) => {
            this.firmServicesList.push({id: item.name, name: item.name});
          });
        }
        this.firmServicesList.push({id: -1, name: this.translates.OTHER_SERVICE});
        this.$scope.$apply();
      })
      .catch((error) => {
        throw new Error(error);
      });
  }

  getFirmUserServices() {
    if (this.id) {
      this.firmUserServiceModel.find(this.id)
        .then((res) => {
          if (res.data.status === 200) {
            this.model = res.data.data;
            console.log('1', _.find(this.firmServicesList, {id: this.model.name}), this.firmServicesList, this.model.name);
            if(!_.find(this.firmServicesList, {id: this.model.name}) ) {
              console.log('+');
              this.model.other_name = this.model.name;
              this.model.name = '-1';
            }
          }
        })
        .catch((error) => {
          throw new Error(error);
        });
    }
  }

  getServices() {
    return this.firmServicesList;
  }

  save() {
    this.isLoaded = false;
    const cloneData = _.clone(this.model);
    cloneData.firm_id = +this.firmId;
    cloneData.user_id = this.userId;
    if (cloneData.name === '-1') {
      cloneData.name = cloneData.other_name;
    }
    this.firmUserServiceModel.save(cloneData)
      .then((data) => {
        if (data.status === 200) {
          this.toastr.success(this.translates.SUCCESSED_SAVE);
          this.model = {};
          this.isLoaded = true;
          this.$rootScope.$broadcast('refreshServiceList');
          this.cancel();
        } else {
          this.isLoaded = true;
          this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
        }
      })
      .catch((error) => {
        console.log('error', error);
        this.isLoaded = true;
        this.errors = error;
        this.toastr.error(this.translates.ERROR_SAVING_FORM_TRY_AGAIN);
        throw new Error(error)
      });
  }

  cancel() {
    this.$uibModalInstance.dismiss('cancel');
  };
}

addFormController
  .$inject = ['$scope', '$rootScope', '$translate', '$uibModalInstance', '$q', 'firmUserServiceModel', 'firmServiceModel', 'toastr'];
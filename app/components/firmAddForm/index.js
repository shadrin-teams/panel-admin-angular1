import directiveContacts from './contacts/directive';
import directiveAbout from './about/directive';
import directiveServices from './services/directive';
import directiveConfirm from './confirm/directive';
import moduleName from '../../module.name';

angular
  .module(moduleName)
  .directive('firmContacts', directiveContacts)
  .directive('firmAbout', directiveAbout)
  .directive('firmServices', directiveServices)
  .directive('firmConfirm', directiveConfirm);
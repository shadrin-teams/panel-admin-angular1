import view from './view.pug';

export default function gridAddButtonDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      title: '@',
      url: '@'
    },
    controllerAs: 'gridAddButtonCT',
  };
}

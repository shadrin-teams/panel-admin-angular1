import view from './view.pug';
import controller from './controller';

export default function wizardStepDirective() {
  return {
    restrict: 'EA',
    template: view,
    scope: {
      steps: '=',
      active: '=',
    },
    controller,
    controllerAs: 'wizardStepCtrl'
  };
}

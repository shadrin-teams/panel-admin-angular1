export default class wizardStepCtrlController {
  constructor($scope) {
    this.url = '';
    this.$scope = $scope;
  }

  $onInit() {
    if (this.$scope.baseUrl) {
      this.url = `${this.$scope.baseUrl}({id: ${this.$scope.baseUrl.params.id})`;
    }
  }
}

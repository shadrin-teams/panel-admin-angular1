import styles from './styles.sass';
import listMenu from '../../listMenu';

export default class leftMenuController {
  constructor() {
    this.isLoadData = false;
    this.listMenu = listMenu;
    this.init();
  }

  init() {
    this.isLoadData = true;
  }

}

leftMenuController.$inject = [];

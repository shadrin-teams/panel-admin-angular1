import Controller from './controller';
import view from './view.pug';

export default function leftMenuDirective() {
  return {
    restrict: 'EA',
    template: view,
    controller: Controller,
    controllerAs: 'leftMenuCT',
  };
}

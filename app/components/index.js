import MODULE_NAME from '../module.name';
import preloaderDirective from './preloader/directive';
import leftMenuDirective from './leftMenu/directive';
import emptyListDirective from './emptyList/directive';
import pageTitleDirective from './pageTitle/directive';
import paginationBlockDirective from './paginationBlock/directive';
import fieldGroupDirective from './fieldGroup/directive';
import wizardStepDirective from './wizardStep/directive';
import gridAddButtonDirective from './gridAddButton/directive';
import firmAddForm from './firmAddForm/directive';

angular
  .module(MODULE_NAME)
  .directive('preloader', preloaderDirective)
  .directive('pageTitle', pageTitleDirective)
  .directive('leftMenu', leftMenuDirective)
  .directive('paginationBlock', paginationBlockDirective)
  .directive('gridAddButton', gridAddButtonDirective)
  .directive('fieldGroup', fieldGroupDirective)
  .directive('wizardStep', wizardStepDirective)
  .directive('firmAddForm', firmAddForm)
  .directive('emptyList', emptyListDirective);

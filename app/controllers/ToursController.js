import gridOptions from "../config/grid.options";

export default class {
  constructor($scope, $log, $translate, restService, API_PUBLIC, API_ENDPOINT, $state, toastr, FileUploader, permitService) {
    $scope.isLoaded = false;
    $scope.listSite = [];
    $scope.cat = $state.params.cat;
    $scope.name = 'Посты';
    $scope.find = {};
    $scope.gridActions = {};
    $scope.publicUrl = API_PUBLIC.url;
    this.$scope = $scope;

    this.$state = $state;
    this.$translate = $translate;
    this.toastr = toastr;
    this.permitService = permitService;
    this.restService = restService;
    this.translateData = {};
    this.gridOptions = gridOptions;
    this.init();
  }
  init() {

    this.gridOptions.appScopeProvider = this;
    // this.gridOptions.gridOptions = `${CONFIG.api.base}/${CONFIG.api.ver}/ranks`;

    this.gridOptions.columnDefs = [
      {field: 'id', displayName: 'ID', type: 'number', width: '5%', enableSorting: true},
      {field: 'title', displayName: 'Title', enableSorting: true,  type: 'string',
        cellTemplate: `<div class="ui-grid-cell-contents"><a ui-sref="toursInfo({id:row.entity.id})">{{row.entity.title}}</a> </div>`
      },
      {
        field: 'more',
        enableSorting: false,
        width: '100',
        cellTemplate: `<div class="ui-grid-cell-contents text-center">
                          <a ui-sref="toursInfo({id:row.entity.id})" class="btn btn-md btn-primary">
                              <i class="fa fa-pencil"></i></a>
                          <a class="btn btn-danger btn-md" ng-click="grid.appScope.delete(row.entity.id)">
                              <i class="fa fa-times"></i></a>
                    </div>`
      }
    ];

    this.$translate(['PERMIT_ERROR'])
      .then(data=>{
          this.translateData = data;
      });

    if (this.permitService.checkPermit('tours', 'GET')) {
      this.restService.get('tours', {})
        .then(response => {
          this.gridOptions.data = response.data.data;
          this.$scope.isLoaded = true;
        })
    } else {
      this.$scope.isLoaded = true;
      this.toastr.error(this.translateData.PERMIT_ERROR);
      this.$state.go('index');
    }
  }

  delete(id) {
    console.log('11');
    if (confirm('Вы подтверждаете удаление?')) {
      restService.delete('site/' + id)
        .then(function(response) {

          if (response.data.status === 'deleted') {
            this.toastr.error('Продукция успешно удаленна');
            loadData();
          }
          else {
            this.toastr.info('Произошла ошибка удаления');
          }
        })
    }
  };
}


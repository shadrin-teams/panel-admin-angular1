import moduleName from '../module.name';
import appController from './AppController';
import toursController from './ToursController';
import loginController from './LoginController';
import firmsController from './FirmsController';
import PermitErrorController from './PermitErrorController';
import firmController from './FirmController';
import indexController from './IndexController';

angular
  .module(moduleName)
  .controller('appController', appController)
  .controller('LoginController', loginController)
  .controller('IndexController', indexController)
  .controller('FirmsController', firmsController)
  .controller('FirmController', firmController)
  .controller('PermitErrorController', PermitErrorController)
  .controller('ToursController', toursController);

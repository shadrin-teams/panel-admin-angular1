export default class {
  constructor($scope, authService, $rootScope, $state, userService) {
    this.authService = authService;
    this.userService = userService;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$scope = $scope;
    this.isLoaded = false;
    this.userStorageData = {};

    this.user = {
      name: '',
      password: ''
    };
    this.init();
  }

  login() {
    this.authService.login(this.user)
      .then(() => {
        this.$rootScope.$broadcast('login', {user: this.user});
        this.$state.go('index');
      })
      .catch(errMsg => {
        throw new Error('Login failed!' + errMsg);
      });
  };

  clearData() {
    this.authService.destroyUserStoreData();
    this.userStoragedata = {};
  }

  restoreData() {
    this.authService.refreshToken()
      .then(data => {
        this.$state.go('index');
      })
      .catch(error => {
        throw new Error(error);
      })
  }

  init() {
    this.authService.getUserStorageData()
      .then(storageData => {
        this.userStoragedata = storageData;
        this.isLoaded = true;
      })
      .catch(error => {
        throw new Error(error);
      })
  }
}
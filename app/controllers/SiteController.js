(function() {

  'use strict';

  angular
    .module('app')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('SiteController', SiteController);
  function SiteController($scope, $log, $http, restService, $state, toastr) {
    console.log(4);
    $scope.id = $state.params.id;
    $scope.model = {};
    $scope.sectionModel = {};
    $scope.formAddSectionIsOpen = false;
    $scope.isLoaded = false;
    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.saveSection = function() {
      if ($scope.sectionModel.name) {
        $scope.sectionModel.site = $scope.id;
        restService.post('section/' + $scope.model.id, {data: $scope.sectionModel})
          .then(function(data) {
            restService.get('section/' + $scope.model.id)
              .then(function(listResponse) {
                $scope.gridOptions.data = listResponse.data.data;
                $scope.formAddSectionIsOpen = false;
                $scope.sectionModel = {};
                toastr.success('Раздел успешно содан');
              })
              .catch(function(error) {
                toastr.error(error);
                console.log('error', error);
              })
          })
          .catch(function(error) {
            toastr.error(error);
            console.log('error', error);
          })
      }
    };

    $scope.saveSite = function() {
      $scope.isLoaded = false;
      if ($scope.model.id) {
        restService.put('site/' + $scope.model.id, {data: $scope.model})
          .then(function(response) {
            if (!response.data.error) {
              console.log('response', response);
              toastr.success('Запись успешно обновленна');
            } else {
              console.log('error', response.data.error);
              toastr.error(response.data.error.message);
            }
            $scope.isLoaded = true;
          })
          .catch(function(error) {
            console.log('error', error);
            toastr.error('Ошибка обновления');
            $scope.isLoaded = true;
            throw error;
          })
      } else {
        restService.post('site', {data: $scope.model})
          .then(function(response) {
            if (!response.data.error) {
              toastr.success('Запись успешно создана');
            } else {
              console.log('error', response.data.error);
              toastr.error(response.data.error.message);
            }
            $scope.isLoaded = true;
          })
          .catch(function(error) {
            console.log('error', error);
            toastr.success('Ошибка создания');
            $scope.isLoaded = true;
            throw error;
          })
      }
    };

    $scope.delete = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        restService.delete('section/' + id)
          .then(function(response) {

            if (response.data.status === 'deleted') {
              toastr.error('Продукция успешно удаленна');
              loadData();
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    };

    function loadData() {
      if ($scope.id) {
        restService.get('site/' + $scope.id)
          .then(function(response) {
            $scope.isLoaded = true;
            $('table.table').show(400);
            $scope.model = response.data.data;

            restService.get('section/' + $scope.id)
              .then(function(responseSection) {
                $scope.gridOptions.data = responseSection.data.data;
              });

          });
      } else {
        $scope.isLoaded = true;
      }
    }
  };
})();
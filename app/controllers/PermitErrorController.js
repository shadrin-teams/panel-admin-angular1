import gridOptions from '../config/grid.options';

export default class {
  constructor($scope, $log, $translate, $state, $stateParams, toastr, messagesModel, authService, ADMIN) {
    $scope.isLoaded = true;
    $scope.form = {};
    $scope.formOpen = true;
    $scope.errors = {};
    this.$scope = $scope;
    this.$stateParams = $stateParams;

    this.$state = $state;
    this.$translate = $translate;
    this.toastr = toastr;
    this.messagesModel = messagesModel;
    this.authService = authService;
    this.ADMIN = ADMIN;
    this.translateData = {};
    this.sectionSlug = '';
    this.init();
  }

  init() {
    this.sectionSlug = this.$stateParams.section;
    this.$translate(['MESSAGE_SENT', 'MESSAGE_NOT_SEND'])
      .then(data=>{
        this.translateData = data;
      });
  }

  send(){
    const data = this.$scope.form;
    data.user_from = this.authService.getUser().id;
    data.user_to = this.ADMIN.id;
    data.date = Date.now();
    data.subject = `Problem with access user#${data.user_from}`;
    data.data = {section: this.sectionSlug};

    if( this.messagesModel.validate(data)){
      this.messagesModel.save(data)
        .then(data=>{
          this.$scope.formOpen = false;
          this.$scope.form = {};
          this.toastr.success(this.translateData.MESSAGE_SEND);
        })
        .catch(()=>{
          this.toastr.error(this.translateData.MESSAGE_NOT_SEND);
        });
    } else {
      this.$scope.errors = this.messagesModel.errors;
      this.toastr.error(this.translateData.MESSAGE_NOT_SEND);
    }
  }
}


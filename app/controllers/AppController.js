export default class {
  constructor($scope, $state, $rootScope, $log, authService, toursModel, userService) {

    $scope.userAuth = authService.isAuthenticated();
    $scope.authService = authService;
    this.userService = userService;
    this.toursModel = toursModel;
    $scope.activeMenu = '';

    this.$rootScope = $rootScope;
    $scope.user = $scope.authService.getUser();
    $scope.userAvatar = this.userService.getAvatar($scope.user);

    $scope.toggle = id => {
      $(id).toggle(200);
    };

    $scope.setActiveMenu = (menuSlug) => {
      if(menuSlug !== $scope.activeMenu ){
        $scope.activeMenu = menuSlug;
      } else {
        $scope.activeMenu = '';
      }
    };

    $scope.logout = () => {
      this.$rootScope.$broadcast('logout', {});
      this.$scope.authService.logout();
      $state.go('login');
    };

    $scope.$on('login', event => {
      $log.info('login');
    });

    $scope.$on('logout', event => {
      $log.info('logout');
      this.$scope.authService.logout();
    });

    this.$scope = $scope;
    this.init();
  }

  init() {
  }
};
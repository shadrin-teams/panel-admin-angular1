(function() {

  'use strict';

  angular
    .module('app')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('SectionController', SectionController);
  function SectionController($scope, $log, $http, restService, $state, toastr) {

    $scope.siteId = $state.params.id;
    $scope.sid = $state.params.sid;
    $scope.model = {};
    $scope.keyModel = {};
    $scope.subModel = {};
    $scope.formAddSectionIsOpen = false;
    $scope.formAddKeywordTypeOne = true;
    $scope.formAddSubSectionIsOpen = false;
    $scope.showSubSection = false;
    $scope.showKeyWord = false;
    $scope.editformSectionIsOpen = false;
    $scope.isLoaded = false;
    $scope.gridOptions = {
      data: [],
      sort: {predicate: 'count',
        direction: 'asc'},
      urlSync: true
    };

    $scope.gridSubOptions = {
      data: [],
      sort: {
      },
      urlSync: true
    };
    loadData();

    $scope.changeFormAddKeywordTypeOne = function(isOne) {
      $scope.formAddKeywordTypeOne = isOne;
    };

    $scope.saveKey = function() {

      // Одиночная вставка
      if ($scope.formAddKeywordTypeOne) {
        if ($scope.keyModel.name) {
          $scope.keyModel.site = $scope.siteId;
          restService.post('keyword/' + $scope.siteId + '/' + $scope.model.id, {data: $scope.keyModel})
            .then(function(data) {
              if(data.data.status === 200) {
                restService.get('keyword/' + $scope.model.id)
                  .then(function(listResponse) {
                    $scope.gridOptions.data = listResponse.data.data;
                    $scope.formAddSectionIsOpen = false;
                    $scope.keyModel = {};
                    toastr.success('Ключ. слово успешно содан');
                  })
                  .catch(function(error) {
                    toastr.error(error);
                    console.log('error', error);
                  })
              } else {
                toastr.error(data.data.error);
                console.log('error', (data.data.error));
              }

            })
            .catch(function(error) {
              toastr.error(error);
              console.log('error', error);
            })
        } else {
          toastr.error('Не заполненный ключевые слова');
        }
      } else {
        if ($scope.keyModel.list) {

          var text = $scope.keyModel.list;
          var lines = text.split('\n');
          var params = [];
          var result = [];
          var value = '';
          for (var i = 0; i < lines.length; i++) {
            params = lines[i].split('\t');
            params[1] = params[1].replace(/([ ])*/g, '');
            console.log(params[0], ' - ', parseInt(params[1]));
            result.push({name: params[0], value: parseInt(params[1])});
          }

          restService.post('keyword-list/' + $scope.siteId + '/' + $scope.model.id, {data: result})
            .then((data) => {
              if (data.data.countAdd > 0) {
                toastr.success('Успешно внесенно ' + data.data.countAdd);
                $scope.keyModel.list = '';
                $scope.formAddSectionIsOpen = false;

                restService.get('keyword/' + $scope.sid)
                  .then(function(responseSection) {
                    $scope.gridOptions.data = responseSection.data.data;
                  });
              }
              if (data.data.countAdd < result.length) {
                toastr.error('Не внесенны ' + (result.length - data.data.countAdd));
                toastr.error(data.data.errors);
              }
            })
            .catch((error) => {
              toastr.error(error);
            });

        } else {
          toastr.error('Не заполненный ключевые слова');
        }
      }

    };

    $scope.saveSubSection = function() {
      $scope.isLoaded = false;
      restService.post('sub-section/' + $scope.siteId + '/' + $scope.model.id, {data: $scope.subModel})
        .then(function(response) {
          if (!response.data.error) {
            console.log('response', response);
            toastr.success('Запись успешно создана');

            restService.get('sub-section/' + $scope.model.id)
              .then(function(responseSubSection) {
                $scope.subModel = {};
                $scope.formAddSubSectionIsOpen = false;
                $scope.gridSubOptions.data = responseSubSection.data.list;
              });

          } else {
            console.log('error', response.data.error);
            toastr.error(response.data.error.message);
          }
          $scope.isLoaded = true;
        })
        .catch(function(error) {
          console.log('error', error);
          toastr.error('Ошибка обновления');
          $scope.isLoaded = true;
          throw error;
        })

    };

    $scope.saveSection = function() {
      $scope.isLoaded = false;
      restService.put('section/' + $scope.model.id, {data: $scope.model})
        .then(function(response) {
          if (!response.data.error) {
            console.log('response', response);
            toastr.success('Запись успешно обновленна');
          } else {
            console.log('error', response.data.error);
            toastr.error(response.data.error.message);
          }
          $scope.isLoaded = true;
        })
        .catch(function(error) {
          console.log('error', error);
          toastr.error('Ошибка обновления');
          $scope.isLoaded = true;
          throw error;
        })

    };

    $scope.delete = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        restService.delete('keyword/' + id)
          .then(function(response) {

            if (response.data.status === 'deleted') {

              restService.get('keyword/' + $scope.sid)
                .then(function(responseSection) {
                  $scope.gridOptions.data = responseSection.data.data;
                  toastr.success('Запись успешно удаленна');
                })
                .catch( function(error){
                  toastr.error('Ошибка удаление ключ. слова');
                  toastr.error(error);
              })
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    };

    $scope.deleteSection = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        restService.delete('section/' + id)
          .then(function(response) {

            if (response.data.status === 'deleted') {

              restService.get('section/' + $scope.sid)
                .then(function(responseSection) {
                  $scope.gridSubOptions.data = responseSection.data.data;
                  toastr.success('Запись успешно удаленна');
                })
                .catch( function(error){
                  toastr.error('Ошибка удаление');
                  toastr.error(error);
              })
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    };

    function loadData() {
      if ($scope.siteId) {
        restService.get('section/' + $scope.siteId + '/' + $scope.sid)
          .then(function(response) {
            $scope.isLoaded = true;
            $('table.table').show(400);
            $scope.model = response.data.data;

            restService.get('keyword/' + $scope.sid)
              .then(function(responseSection) {
                $scope.gridOptions.data = responseSection.data.data;
                if ($scope.gridOptions.data.length) {
                  $scope.showKeyWord = true;
                }

                restService.get('sub-section/' + $scope.sid)
                  .then(function(responseSubSection) {
                    $scope.gridSubOptions.data = responseSubSection.data.list;
                    if ($scope.gridSubOptions.data.length) {
                      $scope.showSubSection = true;
                    }
                  });

              });

          });
      } else {
        $state.go('sites');
      }
    }
  }
})();
import gridOptions from '../config/grid.options';

export default class {
  constructor($scope, $log, $translate, $state, toastr, firmsModel) {
    $scope.listSite = [];
    $scope.find = {};
    $scope.gridActions = {};
    this.$scope = $scope;

    this.$state = $state;
    this.$translate = $translate;
    this.toastr = toastr;
    this.firmsModel = firmsModel;
    this.translateData = {};
    this.gridOptions = gridOptions;
    this.isLoaded = true;
    this.init();
  }

  init() {
    this.gridOptions.appScopeProvider = this;
    this.gridOptions.columnDefs = [
      {field: 'id', displayName: 'ID', type: 'number', width: '5%', enableSorting: true},
      {field: 'title', displayName: 'Title', enableSorting: true,  type: 'string',
        cellTemplate: `<div class="ui-grid-cell-contents"><a ui-sref="firmsInfo({id:row.entity.id})">{{row.entity.title}}</a> </div>`
      },
      {
        field: 'more',
        enableSorting: false,
        width: '100',
        cellTemplate: `<div class="ui-grid-cell-contents text-center">
                          <a ui-sref="firmsInfo({id:row.entity.id})" class="btn btn-md btn-primary">
                              <i class="fa fa-pencil"></i></a>
                          <a class="btn btn-danger btn-md" ng-click="grid.appScope.delete(row.entity.id)">
                              <i class="fa fa-times"></i></a>
                    </div>`
      }
    ];


      this.firmsModel.findAll()
        .then(response => {
          this.gridOptions.data = response.data.data;
          this.isLoaded = true;
          this.$scope.$apply();
        })
        .catch(error => {
          if(error === 403){
            this.$state.go('permit_error', {section:'firms'});
          }

          this.isLoaded = true;
          // throw new Error(error);
        })
  }

  delete(id) {
    console.log('11');
    if (confirm('Вы подтверждаете удаление?')) {
      restService.delete('site/' + id)
        .then(function(response) {

          if (response.data.status === 'deleted') {
            this.toastr.error('Продукция успешно удаленна');
            loadData();
          }
          else {
            this.toastr.info('Произошла ошибка удаления');
          }
        })
    }
  };
}


import moduleName from '../module.name';
import translateConfig from './translate';
import angularTooltipsConfig from './angular-tooltips.config';
import httpInterceptor from './http.interceptor';
import angularToastrConfig from './angular-toastr.config';

angular
  .module(moduleName)
  .config(translateConfig)
  .config(angularTooltipsConfig)
  .config(angularToastrConfig)
  .factory('httpInterceptor', httpInterceptor)
  .config($httpProvider => {
    $httpProvider.interceptors.push('httpInterceptor');
  })
/*  .factory('$exceptionHandler', ['$log', 'errorService', ($log, errorService) => {
    return function myExceptionHandler(exception, cause) {
      // errorService.sendError('APP_ERROR', exception, '', {});
      $log.warn(exception, cause);
    };
  }])*/;
export default function angularTooltipsConfig(tooltipsConfProvider) {
  tooltipsConfProvider.configure({
    'smart': true,
    'size': 'large',
    'tooltipTemplateUrlCache': true,
    'class': 'greenTip'
  });
}
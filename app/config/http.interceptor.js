export default ($rootScope, $q, $state, AUTH_EVENTS) => {
  return {
    response: (response) => {
      if (response.data.status === 401) {
        $rootScope.$broadcast('ERROR_NOT_AUTHENTICATE');
        return $q.reject(response);
      }
      return $q.resolve(response);
    },
    responseError: (response) => {
      if (response.data.status === 401) {
        $rootScope.$broadcast('RESPONSE_STATUS_401', response);
      }
      return $q.reject(response);
    }
  };
}
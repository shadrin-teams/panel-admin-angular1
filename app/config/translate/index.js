import ru from './translate.ru.json';

export default function translateConfig($translateProvider) {
  $translateProvider
    .translations('ru', ru)
    .preferredLanguage('ru')
    .useSanitizeValueStrategy(null);
}

translateConfig.injct = ['$translateProvider'];
export default {
  // url: `${CONFIG.api.base}/${CONFIG.api.ver}/ranks`,
  enableRowSelection: true,
  enableSelectAll: true,
  selectionRowHeaderWidth: 56,
  enableScrollbars: false,
  enableColumnMenus: false,
  enableHorizontalScrollbar: 0,
  enableVerticalScrollbar: 0,
  enablePaginationControls: false,
  rowHeight: 80,
  paginationPageSize: 10,
  // rowTemplate: '<div ng-click="grid.appScope.ranksVm.openEdit(row.entity.id)" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }" ui-grid-cell></div>',
}
import '../js/jquery-ui-1.12.1.custom/jquery-ui.min';
import 'angular';
import 'angular-translate/dist/angular-translate.min';
import '@uirouter/angularjs';
import 'angular-sanitize/angular-sanitize.min';
import 'angular-ui-bootstrap/dist/ui-bootstrap';
import 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js';
import 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js';
import 'angular-ui-sortable/dist/sortable.min';

/*import 'moment/locale/zh-cn';
 import 'moment/locale/en-gb';
 import 'moment/locale/ru';*/
import 'moment';

import 'lodash';
import '../bower_components/froala-wysiwyg-editor/js/froala_editor.min.js';
import 'angular-froala/src/angular-froala.js';
// import 'angular-animate/angular-animate.min.js';
import '../vendors/bootstrap/dist/js/bootstrap.min.js';
import 'angular-file-upload/dist/angular-file-upload.js';
import 'angular-toastr/dist/angular-toastr.js';
import 'angular-toastr/dist/angular-toastr.tpls.min.js';
import 'angular-tooltips/dist/angular-tooltips.min.css';
import 'angular-tooltips/dist/angular-tooltips.min';

import 'ng-file-upload/dist/ng-file-upload.min.js';

// select2
import '../js/select/select2.full.js';

// Custom Theme Scripts
import '../js/custom.js';

// Include Froala Editor Plugins
import '../bower_components/froala-wysiwyg-editor/js/plugins/align.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/char_counter.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/code_beautifier.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/code_view.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/colors.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/emoticons.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/entities.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/file.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/font_family.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/font_size.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/fullscreen.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/image.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/image_manager.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/inline_style.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/line_breaker.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/link.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/lists.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/paragraph_format.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/paragraph_style.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/quote.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/save.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/table.min.js';
import '../bower_components/froala-wysiwyg-editor/js/plugins/video.min.js';
// End Froala

import 'angular-ui-grid/ui-grid.min.css';
import 'angular-ui-grid/ui-grid.min.js';

// FastClick
import '../vendors/fastclick/lib/fastclick.js';
// NProgress
import '../vendors/nprogress/nprogress.js';
// Chart.js
import '../vendors/Chart.js/dist/Chart.min.js';
// gauge.js
import 'gauge.js/dist/gauge.min.js';
// bootstrap-progressbar
import '../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js';
// iCheck
// import '../vendors/iCheck/icheck.min.js';
// Skycons
// import '../vendors/skycons/skycons.js';
// Flot
import '../vendors/Flot/jquery.flot.js';
import '../vendors/Flot/jquery.flot.pie.js';
import '../vendors/Flot/jquery.flot.time.js';
import '../vendors/Flot/jquery.flot.stack.js';
// import '../vendors/Flot/jquery.flot.resize.js';
// Flot plugins
import '../vendors/flot.orderbars/js/jquery.flot.orderBars.js';
import '../vendors/flot-spline/js/jquery.flot.spline.min.js';
import '../vendors/flot.curvedlines/curvedLines.js';
// DateJS
import '../vendors/DateJS/build/date.js';
// JQVMap
import '../vendors/jqvmap/dist/jquery.vmap.js';
import '../vendors/jqvmap/dist/maps/jquery.vmap.world.js';
import '../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js';
// bootstrap-daterangepicker
import 'bootstrap-daterangepicker/daterangepicker.js';

import '../node_modules/jquery-colorbox/jquery.colorbox-min';
import 'angular-colorbox/js/angular-colorbox.js';
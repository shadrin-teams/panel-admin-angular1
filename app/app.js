import MODULE_NAME from './module.name';

import './vendor';
import './css_vendors';

angular.module(MODULE_NAME, [
  'ui.router',
  'angularFileUpload',
  // 'ngAnimate',
  'toastr',
  'froala',
  'pascalprecht.translate',
  '720kb.tooltips',
  'ngFileUpload',
  'ngSanitize',
  'ui.bootstrap',
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.pagination',
  'ui.grid.selection',
  'ui.grid.treeView',
  'ui.grid.autoResize',
  'ui.grid.expandable',
  'ui.sortable',
  'colorbox',
  'ngQuickDate'])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'app/views/index.html',
        controller: 'IndexController',
        controllerAs: 'indexCtrl'
      })
      .state('permit_error', {
        url: '/permit_error/:section',
        templateUrl: 'app/views/permit_error.html',
        controller: 'PermitErrorController',
        controllerAs: 'permitCtrl'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/views/login.html',
        controller: 'LoginController',
        controllerAs: 'loginCtrl'
      })
      .state('tours', {
        url: '/tours',
        templateUrl: 'app/views/tours.html',
        controller: 'ToursController',
        controllerAs: 'toursCtrl'
      })
      .state('toursInfo', {
        url: '/tours/:id',
        templateUrl: 'app/views/tour.html',
        controller: 'TourController',
        controllerAs: 'tourCtrl'
      })
      .state('firms', {
        url: '/firms',
        templateUrl: 'app/views/firms.html',
        controller: 'FirmsController',
        controllerAs: 'firmsCtrl'
      })
      .state('firm', {
        url: '/firm/:id?/:tab?',
        templateUrl: 'app/views/firm.html',
        controller: 'FirmController',
        controllerAs: 'firmCtrl'
      });

    $urlRouterProvider.otherwise('/login');
  })
  .run(($rootScope, $state, $log, $transitions, $timeout, authService) => {
    $rootScope.appLoad = true;
    console.log('app run');

    $('#preloader').hide();
    $('.main_container').show();

    $rootScope.$on('RESPONSE_STATUS_401', () =>{
      $timeout(() => {
        authService.setAuthenticated(false);
        $state.go('login');
        console.log('ERROR_NOT_AUTHENTICATE', authService.authenticated);
      })
    });

    $transitions.onBefore({}, (trans) => {
      if (!authService.isAuthenticated() && trans.to().name !== 'login' && trans.to().name !== 'register') {
        $timeout(() => {
          $state.go('login')
        })
      }

      if (authService.isAuthenticated() && trans.to().name === 'login') {
        $timeout(() => {
          $state.go('index')
        })
      }

    });

    $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
        if (!angular.isString(error)) {
          error = JSON.stringify(error);
        }
        $log.error('$stateChangeError: ' + error);
      }
    );

  });

require('./load');
angular.module('auth')
    .service('authService', ($cookies, $http, Restangular)  =>{
        'use strict';

        const _this = this;
        this.status = {
            authorized: false,
        };

        this.loginByCredentials = (username, password) => {
            return Restangular.all('sessions').post({ email: username, password: password })
                .then((response) => {
                    return _this.loginByToken(response.contents);
                });
        };

        this.loginByToken = (token) => {
            $http.defaults.headers.common['X-Token'] = token;

            return Restangular.all('sessions').get(token)
                .then((response) => {
                    $cookies.accessToken = token;
                    _this.status.authorized = true;
                    return response;
                });
        };

        this.logout = () => {
            _this.status.authorized = false;
            $cookies.accessToken = '';

            Restangular.all('sessions').remove();
        };
    });